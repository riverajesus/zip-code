#capturar estados a partir de cruda
INSERT INTO estados(nombre,clave)
   SELECT d_estado, c_estado FROM cruda WHERE c_estado GROUP BY(c_estado) ORDER BY c_estado ASC;
   
#capturar municipios a partir de cruda
INSERT INTO municipios(nombre, clave, estados_id)
SELECT DISTINCT c.D_mnpio, c.c_mnpio, e.id FROM cruda c 
	LEFT JOIN estados e ON c.d_estado = e.nombre 
	ORDER BY c.d_estado, c.c_mnpio;
	
#Capturar tipos asentamientos
INSERT INTO asentamientos_tipos(nombre,clave)
SELECT DISTINCT d_tipo_asenta, c_tipo_asenta FROM cruda ORDER BY c_tipo_asenta;	

#Capturar ciudades
INSERT INTO ciudades(nombre, clave, municipios_id)
SELECT DISTINCT d_ciudad, c_cve_ciudad, m.id FROM cruda c
INNER JOIN estados e ON e.clave = c.c_estado
INNER JOIN municipios m ON m.clave = c.c_mnpio AND e.id = m.estados_id;

#Capturar asentamientos
INSERT INTO asentamientos(nombre,codigo,d_cp,id_asenta_cpcons,d_zona,asentamientos_tipos_id,municipios_id,ciudades_id)
SELECT DISTINCT c.d_asenta, c.d_codigo, c.d_CP, c.id_asenta_cpcons, c.d_zona, t.id, m.id, ct.id FROM cruda c
INNER JOIN asentamientos_tipos t ON c.c_tipo_asenta = t.clave
INNER JOIN estados e ON e.clave = c.c_estado
INNER JOIN municipios m ON m.clave = c.c_mnpio AND e.id = m.estados_id
LEFT OUTER JOIN ciudades ct ON m.id = ct.municipios_id AND ct.clave = c.c_cve_ciudad;
