<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class MainController extends Controller
{
    function findCode($code){
        $resp = DB::table('asentamientos as a')
        ->join('asentamientos_tipos as t', 'a.asentamientos_tipos_id', '=', 't.id')
        ->join('municipios as m', 'a.municipios_id', '=', 'm.id')
        ->join('estados as e', 'm.estados_id', '=', 'e.id')
        ->leftJoin('ciudades as c', 'a.ciudades_id', '=', 'c.id')
        ->select('a.id_asenta_cpcons as asc_clave', 'a.nombre as asc_', 'a.d_zona as zon_', 't.nombre as asc_tip_', 'a.codigo as asc_code', 'e.nombre as edo_',
                 'e.clave as edo_clave', 'm.nombre as mnpio_', 'm.clave as mnpio_clave', 'c.nombre as ct_','e.cp as edo_cp')
        ->where('a.codigo', $code)
        ->get();
        
        try{
            $res = array(
                "zip_code" => $resp[0]->asc_code,
                "locality" => $resp[0]->ct_,
                "federal_entity" => array(
                    "key" => $resp[0]->edo_clave,
                    "name" => $resp[0]->edo_,
                    "code" => $resp[0]->edo_cp,
                ),
                "settlements" => array(),
                "municipality" => array(
                    "key" => $resp[0]->mnpio_clave,
                    "name" => $resp[0]->mnpio_,
                )
            );
            $lg = count($resp);
            for($x=0; $x<$lg; $x++){
                $arr = array(
                    "key" => $resp[$x]->asc_clave,
                    "name" => $resp[$x]->asc_,
                    "zone_type" => $resp[$x]->zon_,
                    "settlement_type" => array(
                        "name" => $resp[$x]->asc_tip_
                    ),
                );
                array_push($res["settlements"],$arr); 
            }
        }catch(\Exception $ex){
            abort(404);
        }
        
        return $res;
    }
}
