
Para el desarrollo del reto lo primero que hice fue insertar la información del archivo proporcionado en una tabla en mysql zipcode.cruda

Para una mejor organización de los datos creé un script para guardar la informacion en un modelo de base de datos de 5 tablas
 
 - asentamientos
 - asentamientos_tipos
 - ciudades
 - estados
 - municipios

En la carpeta /public/database se encuentra el script de la base de datos completa así como también el script para insertar los datos de la 
tabla zipcode.cruda hacia la estructura de 5 tablas.

Para la consulta en el WS usé laravel facade, también hice el despliegue en AWS con EC2 en Ubuntu.


